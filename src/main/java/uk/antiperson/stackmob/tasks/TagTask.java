package uk.antiperson.stackmob.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import uk.antiperson.stackmob.StackMob;
import uk.antiperson.stackmob.tools.extras.GlobalValues;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nathat on 25/07/17.
 */
public class TagTask extends BukkitRunnable {

    private StackMob sm;
    public TagTask(StackMob sm){
        this.sm = sm;
    }

    public void run() {
        // World names to avoid
        List<String> noStackWorldNames = sm.config.getCustomConfig().getStringList("no-stack-worlds");
        
        // Is mythicmob support available
        boolean mythicMobSupportPresent = sm.pluginSupport.getMythicSupport() != null;
        
        // Remove at data options
        int configTagRemoveAtDefault = sm.config.getCustomConfig().getInt("tag.remove-at");
        Map<String, Integer> customTypeRemoveAt = new LinkedHashMap<>();
        
        // Tag format options
        String configTagFormatDefault = sm.config.getCustomConfig().getString("tag.format");
        
        // Show tag options
        boolean configTagShowNearbyEnabled = sm.config.getCustomConfig().getBoolean("tag.show-player-nearby.enabled");
        
        boolean shouldUseProtocolSupportForTagsNearby = configTagShowNearbyEnabled && sm.pluginSupport.isProtocolSupportEnabled() && sm.getVersionId() > 1;
        boolean alwaysVisibleDefaultValue = sm.config.getCustomConfig().getBoolean("tag.always-visible");
        Map<String, Boolean> customTypeAlwaysVisible = new LinkedHashMap<>();
        
        for (World w : Bukkit.getWorlds()) {
            if (noStackWorldNames.contains(w.getName())) continue;
            
            List<Entity> entitiesToUpdateTags = new ArrayList<>();
            for (Entity e : w.getLivingEntities()) {
                if (!e.hasMetadata(GlobalValues.METATAG)) continue;
                
                if (e.getMetadata(GlobalValues.METATAG).size() == 0) {
                    e.setMetadata(GlobalValues.METATAG, new FixedMetadataValue(sm, 1));
                }
                
                String typeString = e.getType().toString();

                int removeAt = customTypeRemoveAt.computeIfAbsent(typeString, string -> determineRemoveAt(string, configTagRemoveAtDefault));
                
                if (e.getMetadata(GlobalValues.METATAG).get(0).asInt() > removeAt) {
                    String format = configTagFormatDefault;
                    if (sm.config.getCustomConfig().isString("custom." + typeString + ".tag.format")) {
                        format = sm.config.getCustomConfig().getString("custom." + typeString + ".tag.format");
                    }

                    // Change if it is a mythic mob.
                    if (mythicMobSupportPresent && sm.pluginSupport.getMythicSupport().isMythicMob(e)) {
                        typeString = sm.pluginSupport.getMythicSupport().getMythicMobs().getMythicMobInstance(e).getType().getInternalName();
                    } else if (sm.translation.getCustomConfig().getBoolean("enabled")) {
                        typeString = "" + sm.translation.getCustomConfig().getString(e.getType().toString());
                    }

                    String formattedType = toTitleCase(typeString.toLowerCase().replace("_", " "));
                    String nearlyFinal = format.replace("%size%", e.getMetadata(GlobalValues.METATAG).get(0).asString())
                                .replace("%type%", formattedType)
                                .replace("%bukkit_type%", e.getType().toString());
                    String finalString = ChatColor.translateAlternateColorCodes('&', nearlyFinal);
                    if(!finalString.equals(e.getCustomName())){
                        e.setCustomName(finalString);
                    }

                    if(shouldUseProtocolSupportForTagsNearby){
                        //sm.pluginSupport.getProtocolSupport().sendUpdatePackets(Bukkit.getOnlinePlayers(), e);
                        // Save this one for tag updating more efficiently at the end
                        entitiesToUpdateTags.add(e);
                        //Bukkit.getOnlinePlayers().forEach(player -> sm.pluginSupport.getProtocolSupport().sendUpdatePacket(player, e));
                    }else{
                        boolean alwaysVisible = customTypeAlwaysVisible.computeIfAbsent(typeString, string -> determineAlwaysVisible(string, alwaysVisibleDefaultValue));
                        e.setCustomNameVisible(alwaysVisible);
                    }
                }
            }
            // Send the packets here if using protocol support
            if (shouldUseProtocolSupportForTagsNearby) {
                sm.pluginSupport.getProtocolSupport().sendUpdatePackets(Bukkit.getOnlinePlayers(), entitiesToUpdateTags);
            }
        }
    }
    
    private int determineRemoveAt(String typeString, int defaultValue) {
        if (sm.config.getCustomConfig().isString("custom." + typeString + ".tag.remove-at")) {
            return sm.config.getCustomConfig().getInt("custom." + typeString + ".tag.remove-at");
        }
        return defaultValue;
    }

    private boolean determineAlwaysVisible(String typeString, boolean defaultValue) {
        if (sm.config.getCustomConfig().isString("custom." + typeString + ".tag.always-visible")) {
            return sm.config.getCustomConfig().getBoolean("custom." + typeString + ".tag.always-visible");
        }
        return defaultValue;
    }
    
    private String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String s : arr){
             sb.append(Character.toUpperCase(s.charAt(0)))
                    .append(s.substring(1)).append(" ");
        }
        return sb.toString().trim();
    }
}
