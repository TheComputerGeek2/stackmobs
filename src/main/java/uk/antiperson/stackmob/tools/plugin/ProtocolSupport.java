package uk.antiperson.stackmob.tools.plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import uk.antiperson.stackmob.StackMob;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ProtocolSupport{

    private ProtocolManager protocolManager;
    private double xLoc;
    private double yLoc;
    private double zLoc;
    public ProtocolSupport(StackMob sm){
        protocolManager = ProtocolLibrary.getProtocolManager();
        zLoc = sm.config.getCustomConfig().getDouble("tag.show-player-nearby.z");
        yLoc = sm.config.getCustomConfig().getDouble("tag.show-player-nearby.y");
        xLoc = sm.config.getCustomConfig().getDouble("tag.show-player-nearby.x");
    }

    // All entities in this must be in the same world
    // No parameters may be null or contain null
    public void sendUpdatePackets(Collection<? extends Player> players, Collection<? extends Entity> entities) {
        // Sanity check to avoid wasting time
        if (entities.isEmpty()) return;
        if (players.isEmpty()) return;
        
        // Sort through the players to see which are near nothing, which are near what,
        // Then send them through optimized routes for update packets
        
        // Players in a different world from the mobs
        // These players can be processed early since they shouldn't be close to any of the mobs
        List<Player> wrongWorld = new ArrayList<>();
        // Players in the same world as the mobs
        List<Player> rightWorld = new ArrayList<>();
        
        World targetWorld = entities.stream().findFirst().get().getWorld();
        
        // Let's sort the players
        for (Player player : players) {
            // Handle the ones in the wrong world
            if (!player.getWorld().equals(targetWorld)) {
                wrongWorld.add(player);
                continue;
            }
            
            // Handle ones in the right world
            rightWorld.add(player);
        }
        
        
        // Send the packets
        for (Entity e : entities) {
            // Wrong world
            sendUpdatePackets(wrongWorld, e, false);
            
            // Right world
            sendUpdatePackets(rightWorld, e);
        }
    }
    
    public void sendUpdatePackets(Collection<? extends Player> players, Entity entity) {
        // Sanity check to avoid wasting time
        if (players.isEmpty()) return;
        
        // Get the nearby player uuids in a set to serve as an efficient cache
        Set<UUID> nearbyPlayerUUIDs = new HashSet<>();
        entity.getNearbyEntities(xLoc, yLoc, zLoc)
            .stream().filter(e -> e instanceof Player)
            .forEach(p -> nearbyPlayerUUIDs.add(p.getUniqueId()));
        
        // Send the update packets
        players.forEach(player -> sendUpdatePacket(player, entity, nearbyPlayerUUIDs.contains(player.getUniqueId())));
    }
    
    public void sendUpdatePacket(Player player, Entity entity) {
        for (Entity e : entity.getNearbyEntities(xLoc, yLoc, zLoc)) {
            if(e.getUniqueId().equals(player.getUniqueId())) {
                sendUpdatePacket(player, entity, true);
                return;
            }
        }
        sendUpdatePacket(player, entity, false);
    }
    
    // Conveinence method, but later optimize this to reuse packets if possible
    public void sendUpdatePackets(Collection<? extends Player> players, Entity entity, boolean show) {
        players.forEach(player -> sendUpdatePacket(player, entity, show));
    }
    
    public void sendUpdatePacket(Player player, Entity entity, boolean show) {
        PacketContainer packet = protocolManager.createPacket(PacketType.Play.Server.ENTITY_METADATA);
        // Cloning the packet and getting the entity involved.
        WrappedDataWatcher watcher = new WrappedDataWatcher(entity);
        WrappedDataWatcher.Serializer booleanSerializer = WrappedDataWatcher.Registry.get(Boolean.class);
        // Set if the tag is visible or not.
        watcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, booleanSerializer), show);
        
        // Writing the stuff to the packet.
        packet.getEntityModifier(entity.getWorld()).write(0, entity);
        packet.getWatchableCollectionModifier().write(0, watcher.getWatchableObjects());

        // Send the new packet.
        try{
            protocolManager.sendServerPacket(player, packet);
        }catch (InvocationTargetException e){
            e.printStackTrace();
        }
    }
}
